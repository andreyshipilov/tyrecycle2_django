######################################
# tyrecycle2 staging
######################################

<Virtualhost *:80>
    ServerAdmin noreply@svelteteam.com
    ServerName tyrecycle2.svelteteam.com

    CustomLog /var/log/apache2/svelteteam/tyrecycle2.log vhost_combined
    ErrorLog /var/log/apache2/svelteteam/tyrecycle2.error.log
    ServerSignature On

    WSGIDaemonProcess tyrecycle2_staging python-path=/var/www/svelteteam/tyrecycle2_staging/venv/lib/python2.7/site-packages:/var/www/svelteteam/tyrecycle2_staging:/var/www/svelteteam/tyrecycle2_staging/tyrecycle2_django
    WSGIProcessGroup tyrecycle2_staging
    WSGIScriptAlias / /var/www/svelteteam/tyrecycle2_staging/tyrecycle2_django/tyrecycle2/wsgi/staging.py

    Alias /s /var/www/svelteteam/tyrecycle2_staging/tyrecycle2_django/static_root
    Alias /m /var/www/svelteteam/tyrecycle2_staging/tyrecycle2_django/media_root
</Virtualhost>
