######################################
# tyrecycle2 production
######################################

<Virtualhost *:80>
    ServerAdmin noreply@svelteteam.com
    ServerName www.tyrecycle.com.au
    ServerAlias tyrecycle.com.au

    CustomLog /var/log/apache2/tyrecycle2.log vhost_combined
    ErrorLog /var/log/apache2/tyrecycle2.error.log
    ServerSignature On

    WSGIDaemonProcess tyrecycle2 python-path=/var/www/tyrecycle2/venv/lib/python2.7/site-packages:/var/www/tyrecycle2:/var/www/tyrecycle2/tyrecycle2_django
    WSGIProcessGroup tyrecycle2
    WSGIScriptAlias / /var/www/tyrecycle2/tyrecycle2_django/tyrecycle2/wsgi/prod.py

    # Alias for favicon and static files.
    Alias /favicon.ico /var/www/tyrecycle2/tyrecycle2_django/static_root/img/favicon.ico
    Alias /apple-touch-icon.png /var/www/tyrecycle2/tyrecycle2_django/static_root/img/apple-touch-icon.png
    Alias /s /var/www/tyrecycle2/tyrecycle2_django/static_root
    Alias /m /var/www/tyrecycle2/tyrecycle2_django/media_root

    RewriteEngine on
    RewriteCond %{HTTP_HOST} !^www.tyrecycle.com.au$ [NC]
    RewriteRule ^(.*)$ http://www.tyrecycle.com.au$1 [R=301,L]

    <Directory /var/www/tyrecycle2>
        Options -Indexes
        Order allow,deny
        Allow from all
    </Directory>

    <FilesMatch "\.(ico|jpg|jpeg|png|gif|js|css|svg|woff|woff2|eot|ttf)$">
        Header set Cache-Control "max-age=290304000, public"
    </FilesMatch>

    <IfModule mod_deflate.c>
        AddOutputFilterByType DEFLATE text/text text/html text/plain text/xml text/css application/x-javascript application/javascript application/json image/svg+xml
    </IfModule>
</Virtualhost>
