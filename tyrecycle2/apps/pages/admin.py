from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin
from reversion.admin import VersionAdmin

from .models import Page


@admin.register(Page)
class PageAdmin(SortableAdminMixin, VersionAdmin):
    list_display = ('__unicode__', 'parent', 'is_published')
    search_fields = ('title', 'short_description', 'text')
    list_filter = ('is_published',)
    fieldsets = (
        (None, {
            'fields': ('is_published', 'parent',)
        }),
        ('Title and texts', {
            'fields': ('title', 'short_description', 'text'),
        }),
        ('Images', {
            'fields': ('image_1', 'image_2', 'image_3',),
        }),
        ('Meta data', {
            'classes': ('collapse',),
            'fields': ('meta_title_db', 'meta_description_db'),
        }),
    )

    def get_form(self, request, obj=None, **kwargs):
        """
        Filter only pages without parents.
        """

        form = super(PageAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['parent'].queryset = Page.get_root_pages()

        return form
