from .models import Page


def pages_middleware(request):
    return {
        'PAGES_ROOT_MENU': Page.get_root_pages(),
    }
