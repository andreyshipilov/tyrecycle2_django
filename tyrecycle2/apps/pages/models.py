# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from datetime import datetime
from os.path import splitext

from django.db import models
from django.core.cache import cache
from django.utils.html import strip_tags
from django.utils.functional import cached_property
from autoslug.fields import AutoSlugField
from any_imagefield.models import AnyImageField
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField
import reversion


def pages_images(instance, filename):
    """Image uploads custom handler."""
    return 'pages-images/{year}/{name}{ext}'.format(
        year=datetime.today().year,
        name=datetime.today().strftime('%m%d%H%M%S%f'),
        ext=splitext(filename)[1].lower()
    )


@reversion.register()
class Page(models.Model):
    is_published = models.BooleanField(default=False,
                                       help_text='Only items set as published will be shown on the site.')
    parent = models.ForeignKey('self',
                               related_name='children',
                               blank=True,
                               null=True,
                               on_delete=models.SET_NULL)
    title = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='title',
                         always_update=True,
                         unique=True)
    short_description = RichTextField(blank=True)
    text = RichTextUploadingField(blank=True)
    image_1 = AnyImageField(
        upload_to=pages_images,
        max_length=250,
        blank=True,
        help_text='The bigger image the better. It is going to be cropped and compressed automatically.')
    image_2 = AnyImageField(
        upload_to=pages_images,
        max_length=250,
        blank=True,
        help_text='The bigger image the better. It is going to be cropped and compressed automatically.')
    image_3 = AnyImageField(
        upload_to=pages_images,
        max_length=250,
        blank=True,
        help_text='The bigger image the better. It is going to be cropped and compressed automatically. \
        Not used on child pages.')
    meta_title_db = models.CharField(max_length=255,
                                     blank=True,
                                     verbose_name='meta title',
                                     help_text='If empty, title will be used')
    meta_description_db = models.CharField(max_length=255,
                                           blank=True,
                                           verbose_name='meta description',
                                           help_text='If empty, truncated page text will be used')

    # show_on_homepage = models.BooleanField(default=False, help_text='Will be shown in the Primary Areas block.')

    order = models.PositiveIntegerField(default=0,
                                        null=False,
                                        db_index=True)

    class Meta:
        ordering = ('order',)

    def __unicode__(self):
        """Returns a readable title for Admin."""
        if self.parent:
            return u'— {0} / {1}'.format(self.parent.title, self.title)
        return self.title

    def save(self, *args, **kwargs):
        """Clear cache on every save."""
        cache.clear()
        super(Page, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        """Returns a corresponding absolute URL for parent/child pages."""
        if self.parent:
            return ('child_page', (), {'parent_slug': self.parent.slug, 'child_slug': self.slug})
        else:
            return ('parent_page', (), {'parent_slug': self.slug})

    @staticmethod
    def get_published():
        return Page.objects.filter(is_published=True)

    @staticmethod
    def get_root_pages():
        return Page.get_published().filter(parent__isnull=True)

    def get_published_children(self):
        return Page.objects.filter(parent=self, is_published=True).prefetch_related('parent')

    @cached_property
    def has_published_children(self):
        return self.get_published_children().exists()

    @cached_property
    def next_by_order(self):
        """Returns next page by order."""
        try:
            obj = Page.get_published().filter(parent=self.parent, order__gt=self.order)[0]
        except IndexError:
            obj = Page.get_published().filter(parent=self.parent).first()

        return obj

    @cached_property
    def previous_by_order(self):
        """Returns previous page by order."""
        try:
            obj = Page.get_published().filter(parent=self.parent, order__lt=self.order).reverse()[0]
        except IndexError:
            obj = Page.get_published().filter(parent=self.parent).last()

        return obj

    @cached_property
    def meta_title(self):
        return self.meta_title_db or self.title

    @cached_property
    def meta_description(self):
        return self.meta_description_db or strip_tags(self.short_description)[:140]
