from haystack import indexes

from .models import Page


class PeriodicalIndex(indexes.SearchIndex, indexes.Indexable):
    indexed_text = indexes.EdgeNgramField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    text = indexes.CharField(model_attr='text')
    short_text = indexes.CharField(model_attr='short_description')

    def get_model(self):
        return Page

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().get_published()
