from django.contrib.sitemaps import Sitemap

from .models import Page


class PageSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.7

    def items(self):
        return Page.get_published()
