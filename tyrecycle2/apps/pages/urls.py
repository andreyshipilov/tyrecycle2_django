from django.conf.urls import url

import views


urlpatterns = [
    url(r'(?P<parent_slug>[-_\w]+)/(?P<child_slug>[-_\w]+)/$', views.page, name='child_page'),
    url(r'(?P<parent_slug>[-_\w]+)/$', views.page, name='parent_page'),
]
