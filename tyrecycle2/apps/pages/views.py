from itertools import chain

from django.shortcuts import render, get_object_or_404
from django.views.decorators.cache import cache_page
from meta.views import Meta

from .models import Page


@cache_page(600)
def page(request, parent_slug=None, child_slug=None):
    if child_slug:
        object = get_object_or_404(Page.get_published(), slug=child_slug, parent__slug=parent_slug)
        meta = Meta(
            title=object.meta_title,
            description=object.meta_description,
            url=object.get_absolute_url(),
            image=object.image_1.url if object.image_1 else None
        )

        # object_blocks = list(chain(object.text_blocks.all(), ))
        # object_blocks_sorted = sorted(object_blocks, key=lambda x: x.order)

        # try:
        #     previous = Page.get_published().filter(parent=object.parent, order__gt=object.order)[0]
        # except:
        #     previous = Page.get_published().filter(parent=object.parent).first()
        #
        # try:
        #     next = Page.get_published().filter(parent=object.parent, order__lt=object.order).reverse()[0]
        # except:
        #     next = Page.get_published().filter(parent=object.parent).last()

        context = {
            'object': object,
            # 'object_blocks': object_blocks_sorted,
            # 'previous': previous,
            # 'next': next,
            'meta': meta,
        }

        return render(request, 'article.html', context)
    else:
        object = get_object_or_404(Page.get_published(), slug=parent_slug, parent__isnull=True)
        meta = Meta(
            title=object.title,
            description=object.short_description,
            url=object.get_absolute_url(),
            image=object.image_1.url if object.image_1 else None
        )

        context = {
            'object': object,
            'meta': meta,
        }

        return render(request, 'pages/parent_page.html', context)
