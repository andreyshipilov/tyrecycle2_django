from django.contrib import admin
from reversion.admin import VersionAdmin

from .models import NewsItem


@admin.register(NewsItem)
class NewsItemAdmin(VersionAdmin):
    list_display = ('__unicode__', 'publication_date', 'is_published')
    date_hierarchy = 'publication_date'
    search_fields = ('title', 'short_description', 'text')
    list_filter = ('is_published',)
    fieldsets = (
        (None, {
            'fields': ('is_published', 'publication_date',)
        }),
        ('Title and texts', {
            'fields': ('title', 'short_description', 'text'),
        }),
        ('Images', {
            'fields': ('image_1', 'image_2',),
        }),
        ('Meta data', {
            'classes': ('collapse',),
            'fields': ('meta_title_db', 'meta_description_db'),
        }),
    )
