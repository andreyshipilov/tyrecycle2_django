# -*- coding: utf-8 -*-
from django.utils.html import escape

from datetime import datetime
from os.path import splitext

from django.db import models
from django.core.cache import cache
from django.utils.functional import cached_property
from django.utils.html import strip_tags
from autoslug.fields import AutoSlugField
from any_imagefield.models import AnyImageField
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField


def periodical_images(instance, filename):
    """Image uploads custom handler."""
    return 'periodical-images/{year}/{name}{ext}'.format(
        year=datetime.today().year,
        name=datetime.today().strftime('%m%d%H%M%S%f'),
        ext=splitext(filename)[1].lower()
    )


class NewsItem(models.Model):
    is_published = models.BooleanField(default=False,
                                       help_text='Only items set as published will be shown on the site.')
    publication_date = models.DateTimeField()
    title = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='title', always_update=True, unique=True)
    short_description = RichTextField(blank=True)
    text = RichTextUploadingField(blank=True)
    image_1 = AnyImageField(
        upload_to=periodical_images,
        max_length=250,
        blank=True,
        help_text='The bigger image the better. It is going to be cropped and compressed automatically.')
    image_2 = AnyImageField(
        upload_to=periodical_images,
        max_length=250,
        blank=True,
        help_text='The bigger image the better. It is going to be cropped and compressed automatically.')

    meta_title_db = models.CharField(max_length=255,
                                     blank=True,
                                     verbose_name='meta title',
                                     help_text='If empty, title will be used')
    meta_description_db = models.CharField(max_length=255,
                                           blank=True,
                                           verbose_name='meta description',
                                           help_text='If empty, truncated page text will be used')

    class Meta:
        ordering = ('-publication_date',)

    def __unicode__(self):
        """Returns a readable title for Admin."""
        return self.title

    def save(self, *args, **kwargs):
        """Clear cache on every save."""
        cache.clear()
        super(NewsItem, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        """Returns a corresponding absolute URL."""
        return ('news_item', (), {'slug': self.slug})

    @staticmethod
    def get_published():
        return NewsItem.objects.filter(is_published=True, publication_date__lte=datetime.today())

    @cached_property
    def meta_title(self):
        return self.meta_title_db or self.title

    @cached_property
    def meta_description(self):
        return self.meta_description_db or strip_tags(self.short_description)[:140]
