from django.conf.urls import url

import views


urlpatterns = [
    url(r'^$', views.news_index, name='news_index'),
    url(r'^(?P<slug>[-_\w]+)/$', views.news_item, name='news_item'),
]
