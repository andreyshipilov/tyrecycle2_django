from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse

from meta.views import Meta
from .models import NewsItem


def news_index(request):
    objects = NewsItem.get_published()
    meta = Meta(
        title='News',
        description='Latest news',
        url=reverse('news_index'),
    )
    context = {
        'objects': objects,
        'meta': meta,
        'is_news_item': True,
    }
    return render(request, 'article.html', context)


def news_item(request, slug):
    object = get_object_or_404(NewsItem.get_published(), slug=slug)
    meta = Meta(
        title=object.meta_title,
        description=object.meta_description,
        url=object.get_absolute_url(),
        image=object.image_1.url if object.image_1 else None
    )
    context = {
        'object': object,
        'meta': meta,
        'is_news_item': True,
    }
    return render(request, 'article.html', context)
