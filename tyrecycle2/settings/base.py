"""
Django settings for tyrecycle2 project.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
import sys
from django.conf.global_settings import STATICFILES_FINDERS


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'bpbq46h-7wyx89bxsrp5hcg0_ykf_9jg+mw22c$&b1e1)3b%we'

SITE_ID = 1

DJANGO_APPS = (
    # 'djangocms_admin_style',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

THIRD_PARTY_APPS = (
    'any_imagefield',
    'compressor',
    'clear_cache',
    'typogrify',
    # 'markdown_deux',
    'robots',
    'sorl.thumbnail',
    'meta',
    'adminsortable2',
    'ckeditor',
    'reversion',
    'periodical',
)

LOCAL_APPS = (
    'pages',
    'django_cleanup',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'tyrecycle2.urls'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/
LANGUAGE_CODE = 'en'
TIME_ZONE = 'Australia/Adelaide'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images) and Media files
# https://docs.djangoproject.com/en/1.9/howto/static-files/
STATIC_URL = '/s/'
STATIC_ROOT = os.path.join(BASE_DIR, '../static_root')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
STATICFILES_FINDERS += (
    'compressor.finders.CompressorFinder',
)
MEDIA_URL = '/m/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../media_root')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [
            os.path.join(BASE_DIR, 'templates')
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.request',
                # 'django.template.context_processors.tz',
                'pages.context_processors.pages_middleware',
            ],
        },
    },
]

# Django compressor settings
# http://django-compressor.readthedocs.org/en/latest/settings/
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)

# Sorl Thumbnail.
THUMBNAIL_ALTERNATIVE_RESOLUTIONS = [1.5, 2]
THUMBNAIL_ORIENTATION = False
THUMBNAIL_PRESERVE_FORMAT = True
THUMBNAIL_QUALITY = 70

# CKEditor.
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_UPLOAD_PATH = 'u/'
CKEDITOR_CONFIGS = {
    'default': {
        'format_tags': 'p;h2;h3',
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Format'],
            ['Blockquote', 'Bold', 'Italic', 'Subscript', 'Superscript', '-', 'RemoveFormat', 'NumberedList',
             'BulletedList', '-', 'PasteFromWord'],
            ['Link', 'Unlink', 'Embed', 'Image'],
            ['HorizontalRule', 'SpecialChar', 'SpellChecker'],
            ['Source']
        ],
        'extraPlugins': 'embed',
    }
}

# Django Meta.
META_SITE_PROTOCOL = 'http'
META_SITE_TYPE = 'website'
META_DEFAULT_KEYWORDS = ['Tyrecycle']
META_INCLUDE_KEYWORDS = META_DEFAULT_KEYWORDS
META_USE_OG_PROPERTIES = True
META_USE_TWITTER_PROPERTIES = True
META_USE_GOOGLEPLUS_PROPERTIES = True
META_OG_NAMESPACES = True
META_DEFAULT_IMAGE = STATIC_URL + 'img/og-image.png'
