"""
Development settings.
"""

from base import *


DEBUG = True

THUMBNAIL_DEBUG = True

TEMPLATES[0]['OPTIONS']['debug'] = True

WSGI_APPLICATION = 'tyrecycle2.wsgi.dev.application'

ALLOWED_HOSTS = ['*']

# Database for development.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'dev.db'),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

# Email backend for development.
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS += (
    'debug_toolbar',
)

INTERNAL_IPS = ('127.0.0.1', '0.0.0.0',)

COMPRESS_ENABLED = True
COMPRESS_REBUILD_TIMEOUT = 1
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter']

META_SITE_DOMAIN = 'localhost:8000'
