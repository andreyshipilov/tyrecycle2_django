"""
Production settings.
"""

from staging import *


# Never leave it as False!
DEBUG = False

WSGI_APPLICATION = 'tyrecycle2.wsgi.prod.application'

# Comment that for WWW not to be prepended.
PREPEND_WWW = True

# This should be set to a list of domains used in production.
ALLOWED_HOSTS = []

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(BASE_DIR, '../django_cache'),
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': 'tyrecycle2_prod',
        'USER': 'tyrecycle2_prod',
        'PASSWORD': '',
    }
}

COMPRESS_CSS_FILTERS = [
    'compressor.filters.template.TemplateFilter',
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter',
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.template.TemplateFilter',
    'compressor.filters.jsmin.JSMinFilter',
]

SERVER_EMAIL = 'tech@sveltestudios.com'
