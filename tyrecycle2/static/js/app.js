// Check SVG support.
function supportsSVG() {
    return !!('createElementNS' in document && document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect);
}

var Menu = (function () {
    var $container, $menu, $subMenus, $arrow;

    return {
        'init': function () {
            $container = $('#top');
            $menu = $('.root-menu', $container);
            $subMenus = $('.sub-menus', $container);
            $arrow = $('.arrow');

            if (!supportsSVG()) {
                $arrow.css('background-image', $arrow.css('background-image').replace('.svg', '.png'));
            }

            $('a,.in', $menu).mouseover(function (e) {
                var $that = $(this),
                    left = $that.position().left - 10,
                    id = $that.attr('id').replace('for-sub-menu-', ''),
                    $subMenu = $('#sub-menu-' + id);

                $('.sub-menu', $subMenus).removeClass('active');
                $arrow.removeClass('active');

                if ($subMenu.length) {
                    $subMenu.css({
                        'left': left
                    }).addClass('active').mouseleave(function () {
                        $subMenu.removeClass('active');
                        $arrow.removeClass('active');
                    });
                    $arrow.addClass('active').css('left', left + $that.outerWidth() / 2);
                }
            });
        }
    }
})();

// DOM ready.
$(function () {
    var $imagesToLoad,
        $window = $(window),
        $document = $(document),
        $reveal = $('#reveal'),
        // Fluid videos trickery.
        $allVideos,
        clearVideosDimensions = function () {
            $allVideos.each(function (i, e) {
                $(e).attr('data-aspectRatio', e.height / e.width).removeAttr('height').removeAttr('width');
            });
        },
        makeVideosFluidOnResize = function () {
            var $fluidEl = $('figure'),
                newWidth = $fluidEl.width();

            $allVideos.each(function (i, e) {
                $(e).width(newWidth).height(newWidth * $(e).attr('data-aspectRatio'));
            });
        };

    // Replace SVG backgrounds.
    $imagesToLoad = $('.ripple, #home-image-1, #home-image-2, #home-image-3');

    if (!supportsSVG()) {
        $imagesToLoad.each(function (i, e) {
            $(e).css('background-image', $(e).css('background-image').replace('.svg', '.png'));
        });
    }

    $imagesToLoad.each(function (i, e) {
        $(e).imagesLoaded({
            background: true
        }, function () {
            $(e).on('inview', function () {
                $(e).addClass('visible');
            });
        });
    });


    $('#footer').css({
        'padding-bottom': '+=' + $reveal.outerHeight(true)
    });

    $window.scroll(function () {
        if ($window.scrollTop() + $window.height() === $document.height()) {
            $reveal.addClass('visible');
        } else {
            $reveal.removeClass('visible');
        }
    });

    // Resize videos.
    $allVideos = $("iframe[src^='http://player.vimeo.com'], iframe[src^='https://www.youtube.com'], object, embed");

    clearVideosDimensions();
    $(window).resize(function () {
        makeVideosFluidOnResize();
    });
    window.setTimeout(function () {
        $(window).resize();
    }, 100);

    // Init menu.
    Menu.init();
});
