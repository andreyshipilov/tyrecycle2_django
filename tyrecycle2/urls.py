from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap

# import pages.sitemaps
# import periodical_content.sitemaps
# import website.sitemaps
import views


SITEMAPS = {
    # 'pages': pages.sitemaps.PageSitemap,
    # 'periodical': periodical_content.sitemaps.PeriodicalSitemap,
    # 'static_views': website.sitemaps.StaticViewsSitemap,
}

admin.site.site_header = 'Tyrecycle administration'
admin.autodiscover()
urlpatterns = []

if settings.DEBUG:
    import debug_toolbar


    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        # url(r'^404/', views.custom_404),
    ]

urlpatterns += [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    # url(r'^sitemap\.xml$', sitemap, {'sitemaps': SITEMAPS}),
    url(r'^robots\.txt$', include('robots.urls')),

    url(r'^$', views.home, name='home'),
    # url(r'^search/', include('haystack.urls')),
    url(r'news/', include('periodical.urls')),
    url(r'', include('pages.urls')),
]
