from django.shortcuts import render


def home(request):
    context = {
        'is_home': True,
    }
    return render(request, 'home.html', context)
